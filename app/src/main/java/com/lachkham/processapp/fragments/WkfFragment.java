package com.lachkham.processapp.fragments;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.drawable.ColorDrawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.Toast;

import com.google.android.material.dialog.MaterialAlertDialogBuilder;
import com.google.android.material.navigation.NavigationView;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.lachkham.processapp.CasesListe;
import com.lachkham.processapp.MainActivity;
import com.lachkham.processapp.R;
import com.lachkham.processapp.model.Wkf;

import java.io.IOException;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

import static com.lachkham.processapp.MainActivity.SHARED_PREFS;

/**
 * A fragment representing a list of Items.
 * <p/>
 * Activities containing this fragment MUST implement the {@link OnListFragmentInteractionListener}
 * interface.
 */
public class WkfFragment extends Fragment {


    // TODO: Customize parameter argument names
    private static final String ARG_COLUMN_COUNT = "column-count";
    // TODO: Customize parameters
    private int mColumnCount = 1;
    private OnListFragmentInteractionListener mListener;

    /**
     * Mandatory empty constructor for the fragment manager to instantiate the
     * fragment (e.g. upon screen orientation changes).
     */
    public WkfFragment() {
    }

    // TODO: Customize parameter initialization
    @SuppressWarnings("unused")
    public static WkfFragment newInstance(int columnCount) {
        WkfFragment fragment = new WkfFragment();
        Bundle args = new Bundle();
        args.putInt(ARG_COLUMN_COUNT, columnCount);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mColumnCount = getArguments().getInt(ARG_COLUMN_COUNT);
        }

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_wkf_list, container, false);

        // Set the adapter
        if (view instanceof RecyclerView) {
            Context context = view.getContext();
            RecyclerView recyclerView = (RecyclerView) view;
            if (mColumnCount <= 1) {
                recyclerView.setLayoutManager(new LinearLayoutManager(context));
            } else {
                recyclerView.setLayoutManager(new GridLayoutManager(context, mColumnCount));
            }
            ListCasesTask listCasesTask = new ListCasesTask(loadToken(), getActivity(), mListener, recyclerView);
            listCasesTask.execute();
            NavigationView navigationView = getActivity().findViewById(R.id.nav_view);
            navigationView.setCheckedItem(R.id.nav_home);

        }
        return view;
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnListFragmentInteractionListener) {
            mListener = (OnListFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnListFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    public String loadToken() {
        String token;

        SharedPreferences sharedPreferences = getContext().getSharedPreferences(SHARED_PREFS, MainActivity.MODE_PRIVATE);
        token = sharedPreferences.getString(MainActivity.ACCESS_TOKEN, null);
        return token;
    }


    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p/>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnListFragmentInteractionListener {
        // TODO: Update argument type and name
        void onListFragmentInteraction(Wkf item);
    }


}

class ListCasesTask extends AsyncTask<Void, Void, List<Wkf>> {
    private String token;
    @SuppressLint("StaticFieldLeak")
    private Context context;
    private WkfFragment.OnListFragmentInteractionListener mListener;
    @SuppressLint("StaticFieldLeak")
    private RecyclerView recyclerView;


    ListCasesTask(String token, Context context, WkfFragment.OnListFragmentInteractionListener mListener, RecyclerView recyclerView) {
        this.token = token;
        this.mListener = mListener;
        this.context = context;
        this.recyclerView = recyclerView;
    }


    @Override
    protected List<Wkf> doInBackground(Void... voids) {
        Log.d("Response", "I'm here top");


        OkHttpClient client = new OkHttpClient().newBuilder()
                .build();
        Request request = new Request.Builder()
                .url("http://process.isiforge.tn/api/1.0/isi/case/start-cases")
                .method("GET", null)
                .addHeader("Authorization", "Bearer " + token)
                .build();

        Log.d("Response", "I'm here just before the response waiting list ...");
        try {
            Response response = client.newCall(request).execute();

            if (response.isSuccessful()) {
                Gson gson = new Gson();
                Type wkfType = new TypeToken<ArrayList<Wkf>>() {
                }.getType();
                List<Wkf> wkfList = gson.fromJson(response.body().string(), wkfType);
                Log.d("Response", wkfList.toString());
                return wkfList;
            } else {
                Log.d("Response", response.body().string());
                throw new IOException("Unexpected code " + response);
                //return null;
            }
        } catch (IOException e) {
            Log.d("Response", "error execute");

            e.printStackTrace();
            return null;
        }
    }

    private AlertDialog materialAlertDialogBuilder = null;

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        FrameLayout frameLayout = ((CasesListe) context).findViewById(R.id.fragment_container);
        frameLayout.setVisibility(View.GONE);

        ColorDrawable cd = new ColorDrawable(0x467fe7);
        materialAlertDialogBuilder = new MaterialAlertDialogBuilder(context)
                .setBackground(cd)
                .setView(R.layout.loading_view)
                .setCancelable(false)

                .show();

    }

    @Override
    protected void onPostExecute(List<Wkf> wkfs) {
        super.onPostExecute(wkfs);
        if (wkfs != null) {
            materialAlertDialogBuilder.dismiss();
            MyWkfRecyclerViewAdapter myWkfRecyclerViewAdapter = new MyWkfRecyclerViewAdapter(wkfs, mListener);
            recyclerView.setAdapter(myWkfRecyclerViewAdapter);
            FrameLayout frameLayout = ((CasesListe) context).findViewById(R.id.fragment_container);
            frameLayout.setVisibility(View.VISIBLE);
        } else {
            Toast.makeText(context, "Une erreur inattendue s’est produite!", Toast.LENGTH_LONG).show();
            Intent intent = new Intent(((CasesListe) context), MainActivity.class);
            ((CasesListe) context).startActivity(intent);
        }
    }
}