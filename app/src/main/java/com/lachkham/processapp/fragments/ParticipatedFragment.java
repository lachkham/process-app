package com.lachkham.processapp.fragments;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.drawable.ColorDrawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.Toast;

import com.google.android.material.dialog.MaterialAlertDialogBuilder;
import com.google.android.material.navigation.NavigationView;
import com.lachkham.processapp.CasesListe;
import com.lachkham.processapp.MainActivity;
import com.lachkham.processapp.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;

import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

import static com.lachkham.processapp.MainActivity.SHARED_PREFS;

/**
 * A fragment representing a list of Items.
 * <p/>
 * Activities containing this fragment MUST implement the {@link OnListFragmentInteractionListener}
 * interface.
 */
public class ParticipatedFragment extends Fragment {

    // TODO: Customize parameter argument names
    private static final String ARG_COLUMN_COUNT = "column-count";
    // TODO: Customize parameters
    private int mColumnCount = 1;
    private OnListFragmentInteractionListener mListener;
    private SwipeRefreshLayout swipeContainer;

    /**
     * Mandatory empty constructor for the fragment manager to instantiate the
     * fragment (e.g. upon screen orientation changes).
     */
    public ParticipatedFragment() {
    }

    // TODO: Customize parameter initialization
    @SuppressWarnings("unused")
    public static ParticipatedFragment newInstance(int columnCount) {
        ParticipatedFragment fragment = new ParticipatedFragment();
        Bundle args = new Bundle();
        args.putInt(ARG_COLUMN_COUNT, columnCount);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (getArguments() != null) {
            mColumnCount = getArguments().getInt(ARG_COLUMN_COUNT);
        }

    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_participated_list, container, false);
        NavigationView navigationView = getActivity().findViewById(R.id.nav_view);
        navigationView.setCheckedItem(R.id.nav_suivi);
        swipeContainer = (SwipeRefreshLayout) getActivity().findViewById(R.id.swipeContainer);
        swipeContainer.setEnabled(true);

        // Setup refresh listener which triggers new data loading

        RecyclerView recyclerView = null;
        // Set the adapter
        if (view instanceof RecyclerView) {
            Context context = view.getContext();
            recyclerView = (RecyclerView) view;
            if (mColumnCount <= 1) {
                recyclerView.setLayoutManager(new LinearLayoutManager(context));
            } else {
                recyclerView.setLayoutManager(new GridLayoutManager(context, mColumnCount));
            }
            new ParticipatedTask(loadToken(), getActivity(), mListener, recyclerView, false, swipeContainer).execute();
            RecyclerView finalRecyclerView = recyclerView;
            swipeContainer.setOnRefreshListener(() -> {
                new ParticipatedTask(loadToken(), getActivity(), mListener, finalRecyclerView, true, swipeContainer).execute();
            });
        }


        return view;
    }

    public String loadToken() {
        String token;

        SharedPreferences sharedPreferences = requireActivity().getSharedPreferences(SHARED_PREFS, MainActivity.MODE_PRIVATE);
        token = sharedPreferences.getString(MainActivity.ACCESS_TOKEN, null);
        return token;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnListFragmentInteractionListener) {
            mListener = (OnListFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnListFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p/>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnListFragmentInteractionListener {
        // TODO: Update argument type and name
        void onListFragmentInteraction(JSONObject item);
    }
}

class ParticipatedTask extends AsyncTask<Void, Long, JSONArray> {
    private final String token;
    @SuppressLint("StaticFieldLeak")
    private final Context context;
    private final ParticipatedFragment.OnListFragmentInteractionListener mListener;
    @SuppressLint("StaticFieldLeak")
    private RecyclerView recyclerView;
    private final Boolean isRefreshing;
    private AlertDialog materialAlertDialogBuilder = null;
    @SuppressLint("StaticFieldLeak")
    private SwipeRefreshLayout swipeContainer;


    ParticipatedTask(String token, Context context, ParticipatedFragment.OnListFragmentInteractionListener mListener, RecyclerView recyclerView, Boolean isRefreshing, SwipeRefreshLayout swipeContainer) {
        this.token = token;
        this.mListener = mListener;
        this.context = context;
        this.recyclerView = recyclerView;
        this.isRefreshing = isRefreshing;
        this.swipeContainer = swipeContainer;

    }

    @Override
    protected JSONArray doInBackground(Void... voids) {
        Log.d("Response", "I'm here top");


        OkHttpClient client = new OkHttpClient().newBuilder()
                .build();
        Request request = new Request.Builder()
                .url("http://process.isiforge.tn/api/1.0/isi/cases/participated")
                .method("GET", null)
                .addHeader("Authorization", "Bearer " + token)
                .build();

        Log.d("Response", "I'm here just before the response waiting list ...");
        try {
            Response response = client.newCall(request).execute();
            InputStream inputStream = null;
            if (response.isSuccessful()) {
                assert response.body() != null;
                String resp = response.body().string();
                Log.d("update", "onProgressUpdate: " + new String(resp));
                return new JSONArray(resp);
            } else {
                throw new IOException("Unexpected code " + response);
            }
        } catch (IOException | JSONException e) {

            e.printStackTrace();
            return null;
        }
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        if (!isRefreshing) {
            ColorDrawable cd = new ColorDrawable(0x467fe7);
            FrameLayout frameLayout = ((CasesListe) context).findViewById(R.id.fragment_container);
            frameLayout.setVisibility(View.GONE);

            materialAlertDialogBuilder = new MaterialAlertDialogBuilder(context)
                    .setBackground(cd)
                    .setView(R.layout.loading_view)
                    .setCancelable(false)
                    .show();
        }
    }


    @Override
    protected void onPostExecute(JSONArray jsonArray) {
        super.onPostExecute(jsonArray);

        if (jsonArray != null) {
            ArrayList<JSONObject> jsonObjects = new ArrayList<JSONObject>();
            for (int i = 0; i < jsonArray.length(); i++) {
                try {
                    jsonObjects.add(jsonArray.getJSONObject(i));
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
            MyParticipatedRecyclerViewAdapter myParticipatedRecyclerViewAdapter = new MyParticipatedRecyclerViewAdapter(jsonObjects, mListener);

            if (!isRefreshing) {
                materialAlertDialogBuilder.dismiss();
                FrameLayout frameLayout = ((CasesListe) context).findViewById(R.id.fragment_container);
                frameLayout.setVisibility(View.VISIBLE);
            } else {
                myParticipatedRecyclerViewAdapter.notifyDataSetChanged();
                recyclerView.scheduleLayoutAnimation();
                swipeContainer.setRefreshing(false);
            }

            recyclerView.setAdapter(myParticipatedRecyclerViewAdapter);

        } else {
            Toast.makeText(context, "Une erreur inattendue s’est produite!", Toast.LENGTH_LONG).show();
            Intent intent = new Intent(((CasesListe) context), MainActivity.class);
            (context).startActivity(intent);
        }

    }


}