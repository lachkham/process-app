package com.lachkham.processapp.fragments;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.material.button.MaterialButton;
import com.lachkham.processapp.CasesListe;
import com.lachkham.processapp.Formulaire;
import com.lachkham.processapp.MainActivity;
import com.lachkham.processapp.R;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

import static com.lachkham.processapp.MainActivity.SHARED_PREFS;


public class CaseInformationDialog extends DialogFragment {

    private JSONObject jsonObject;
    private CaseInformationDialog caseInformationDialog;

    public CaseInformationDialog(JSONObject jsonObject) {
        this.jsonObject = jsonObject;
        this.caseInformationDialog = this;
    }

    public CaseInformationDialog() {
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    /*@Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_case_information_dialog, null);
    }
*/
    @NonNull
    @Override
    public Dialog onCreateDialog(@Nullable Bundle savedInstanceState) {
        View layout = getActivity().getLayoutInflater().inflate(R.layout.fragment_case_information_dialog, null, false);
        assert layout != null;
        //build the alert dialog child of this fragment
        AlertDialog.Builder b = new AlertDialog.Builder(getActivity());

        b.setView(layout);


        CaseInformationTask caseInformationTask = new CaseInformationTask(loadToken(), jsonObject, layout, getActivity(), caseInformationDialog);
        caseInformationTask.execute();
        MaterialButton annulerButton = layout.findViewById(R.id.Annuler_Button);

        annulerButton.setOnClickListener(v -> dismiss());

        return b.create();
    }


    public String loadToken() {
        String token;

        SharedPreferences sharedPreferences = requireContext().getSharedPreferences(SHARED_PREFS, MainActivity.MODE_PRIVATE);
        token = sharedPreferences.getString(MainActivity.ACCESS_TOKEN, null);
        return token;
    }

}

class CaseInformationTask extends AsyncTask<Void, Void, JSONObject> {
    private final String token;
    private final JSONObject requestBody;
    private final View layout;
    private final Context context;
    private final CaseInformationDialog caseInformationDialog;


    CaseInformationTask(String token, JSONObject requestBody, View layout, Context context, CaseInformationDialog caseInformationDialog) {
        this.token = token;
        this.requestBody = requestBody;
        this.layout = layout;
        this.context = context;
        this.caseInformationDialog = caseInformationDialog;
    }

    @Override
    protected JSONObject doInBackground(Void... voids) {

        OkHttpClient client = new OkHttpClient().newBuilder()
                .build();
        Request request = null;
        try {
            request = new Request.Builder()
                    .url("http://process.isiforge.tn/api/1.0/isi/cases/" + requestBody.getString("app_uid"))
                    .method("GET", null)
                    .addHeader("Authorization", "Bearer " + token)
                    .build();
        } catch (JSONException e) {
            e.printStackTrace();
        }
        try {
            Response response = client.newCall(request).execute();
            if (response.isSuccessful()) {
                String resp = response.body().string();
                JSONObject jObj = new JSONObject(resp);
                return jObj;
            }
        } catch (IOException | JSONException e) {
            e.printStackTrace();
            return null;

        }

        return null;
    }

    @Override
    protected void onPostExecute(JSONObject jsonObject) {
        super.onPostExecute(jsonObject);
        if (jsonObject != null) {
            Log.d("Response", "onPostExecute: " + jsonObject.toString());

            ((ProgressBar) layout.findViewById(R.id.progressBar2)).setVisibility(View.GONE);
            ((LinearLayout) layout.findViewById(R.id.content)).setVisibility(View.VISIBLE);
            MaterialButton ModifierButton = layout.findViewById(R.id.modifier_Button);
            MaterialButton AnnulerButton = layout.findViewById(R.id.Annuler_Button);
            try {
                ((TextView) layout.findViewById(R.id.creator)).setText(jsonObject.getString("app_init_usr_username"));
                ((TextView) layout.findViewById(R.id.CaseNumber_number)).setText(jsonObject.getString("app_number"));
                ((TextView) layout.findViewById(R.id.etat)).setText(jsonObject.getString("app_status"));
                ((TextView) layout.findViewById(R.id.date)).setText(jsonObject.getString("app_create_date"));
                ((TextView) layout.findViewById(R.id.updatedate)).setText(jsonObject.getString("app_update_date"));
                JSONObject current_task = jsonObject.getJSONArray("current_task").getJSONObject(0);

                ((TextView) layout.findViewById(R.id.tache)).setText(current_task.getString("tas_title"));
                ((TextView) layout.findViewById(R.id.currentUser)).setText(current_task.getString("usr_name"));
                ((TextView) layout.findViewById(R.id.dateInit)).setText(current_task.getString("del_init_date"));
                ((TextView) layout.findViewById(R.id.title)).setText(jsonObject.getString("pro_name"));

                if (!jsonObject.getString("app_status").equals("DRAFT")) {
                    ModifierButton.setVisibility(View.GONE);
                    AnnulerButton.setText("Fermer");
                }

            } catch (JSONException e) {
                e.printStackTrace();
                ModifierButton.setVisibility(View.GONE);
                AnnulerButton.setText("Fermer");
            }


            ModifierButton.setOnClickListener(v -> {

                Log.d("button", "onPostExecute: " + jsonObject.toString());
                Intent intent = new Intent(((CasesListe) context), Formulaire.class);
                try {
                    JSONObject current_task = jsonObject.getJSONArray("current_task").getJSONObject(0);
                    intent.putExtra("tas_uid", current_task.getString("tas_uid"));
                    intent.putExtra("Pro_title", jsonObject.getString("pro_name"));
                    intent.putExtra("Pro_uid", jsonObject.getString("pro_uid"));
                    intent.putExtra("app_uid", jsonObject.getString("app_uid"));
                    Log.d("intent", "onPostExecute: " + current_task.getString("tas_uid"));
                    Log.d("intent", "onPostExecute: " + jsonObject.getString("pro_name"));
                    Log.d("intent", "onPostExecute: " + jsonObject.getString("pro_uid"));
                    Log.d("intent", "onPostExecute: " + jsonObject.getString("app_uid"));
                    context.startActivity(intent);
                    caseInformationDialog.dismiss();
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            });


        } else {
            Toast.makeText(context, "Une erreur inattendue s’est produite!", Toast.LENGTH_LONG).show();
            caseInformationDialog.dismiss();
        }
    }
}

