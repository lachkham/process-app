package com.lachkham.processapp.fragments;

import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.google.android.material.card.MaterialCardView;
import com.lachkham.processapp.Formulaire;
import com.lachkham.processapp.R;
import com.lachkham.processapp.fragments.WkfFragment.OnListFragmentInteractionListener;
import com.lachkham.processapp.model.Wkf;

import java.util.List;

import androidx.recyclerview.widget.RecyclerView;

/**
 * {@link RecyclerView.Adapter} that can display a {@link Wkf} and makes a call to the
 * specified {@link OnListFragmentInteractionListener}.
 * TODO: Replace the implementation with code for your data type.
 */
public class MyWkfRecyclerViewAdapter extends RecyclerView.Adapter<MyWkfRecyclerViewAdapter.ViewHolder> {

    private final List<Wkf> mValues;
    private final OnListFragmentInteractionListener mListener;

    public MyWkfRecyclerViewAdapter(List<Wkf> items, OnListFragmentInteractionListener listener) {
        mValues = items;
        mListener = listener;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.fragment_wkf, parent, false);
        ViewHolder viewHolder = new ViewHolder(view);

        viewHolder.cardView.setOnClickListener(v -> {
            Log.d("Click", mValues.get(viewHolder.getAdapterPosition()) + "");

            Intent intent = new Intent(v.getContext(), Formulaire.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK); // Adds the FLAG_ACTIVITY_NO_HISTORY flag
            intent.putExtra("tas_uid", mValues.get(viewHolder.getAdapterPosition()).getTas_uid());
            intent.putExtra("Pro_title", mValues.get(viewHolder.getAdapterPosition()).getPro_title());
            intent.putExtra("Pro_uid", mValues.get(viewHolder.getAdapterPosition()).getPro_uid());
            v.getContext().startActivity(intent);
        });

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        holder.mItem = mValues.get(position);
        holder.mContentView.setText(mValues.get(position).getPro_title());

        holder.mView.setOnClickListener(v -> {
            if (null != mListener) {
                // Notify the active callbacks interface (the activity, if the
                // fragment is attached to one) that an item has been selected.
                mListener.onListFragmentInteraction(holder.mItem);
            }
        });
    }

    @Override
    public int getItemCount() {
        return mValues.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public final View mView;
        public final TextView mContentView;
        public Wkf mItem;
        public MaterialCardView cardView;

        public ViewHolder(View view) {
            super(view);
            mView = view;
            mContentView = (TextView) view.findViewById(R.id.content);
            cardView = (MaterialCardView) view.findViewById(R.id.cardParticipated);

        }


        @Override
        public String toString() {
            return super.toString() + " '" + mContentView.getText() + "'";
        }
    }


    public interface OnCaseListener {
        void onCaseClick(int position);
    }
}
