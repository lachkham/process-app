package com.lachkham.processapp.fragments;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.google.android.material.card.MaterialCardView;
import com.lachkham.processapp.CasesListe;
import com.lachkham.processapp.R;
import com.lachkham.processapp.fragments.ParticipatedFragment.OnListFragmentInteractionListener;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;

import androidx.fragment.app.FragmentManager;
import androidx.recyclerview.widget.RecyclerView;

public class MyParticipatedRecyclerViewAdapter extends RecyclerView.Adapter<MyParticipatedRecyclerViewAdapter.ViewHolder> {

    private final List<JSONObject> mValues;
    private final OnListFragmentInteractionListener mListener;

    public MyParticipatedRecyclerViewAdapter(List<JSONObject> items, OnListFragmentInteractionListener listener) {
        mValues = items;
        mListener = listener;
    }


    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.fragment_participated, parent, false);
        ViewHolder viewHolder = new ViewHolder(view);

        viewHolder.maCardView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FragmentManager manager = ((CasesListe) v.getContext()).getSupportFragmentManager();
                CaseInformationDialog caseInformationDialog = new CaseInformationDialog(mValues.get(viewHolder.getAdapterPosition()));
                caseInformationDialog.show(manager, "Case Information");
            }
        });
        return viewHolder;
    }

    public void clear() {
        mValues.clear();
        notifyDataSetChanged();
    }

    // Add a list of items -- change to type used
    public void addAll(List<JSONObject> list) {
        mValues.addAll(list);
        notifyDataSetChanged();
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        holder.mItem = mValues.get(position);
        try {
            holder.mIdView.setText(mValues.get(position).getString("app_number"));
            holder.mContentView.setText(mValues.get(position).getString("app_pro_title"));
            holder.mStatus.setText(mValues.get(position).getString("app_status_label"));
            holder.mSubcontent.setText(String.format("Création: %s", mValues.get(position).getString("app_create_date")));
        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

    @Override
    public int getItemCount() {
        return mValues.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public final View mView;
        public final TextView mIdView;
        public final TextView mContentView;
        public final TextView mStatus;
        public final TextView mSubcontent;
        public final MaterialCardView maCardView;
        public JSONObject mItem;

        public ViewHolder(View view) {
            super(view);
            mView = view;
            mIdView = (TextView) view.findViewById(R.id.item_number);
            mContentView = (TextView) view.findViewById(R.id.content);
            mStatus = (TextView) view.findViewById(R.id.status);
            maCardView = (MaterialCardView) view.findViewById(R.id.cardParticipated);
            mSubcontent = (TextView) view.findViewById(R.id.subcontent);
        }

        @Override
        public String toString() {
            return super.toString() + " '" + mContentView.getText() + "'";
        }
    }
}
