package com.lachkham.processapp;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.google.android.material.textfield.TextInputLayout;
import com.lachkham.processapp.model.Token;

import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONTokener;

import java.io.IOException;
import java.util.concurrent.ExecutionException;

import androidx.appcompat.app.AppCompatActivity;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

public class MainActivity extends AppCompatActivity {

    public static final String SHARED_PREFS = "shared_prefs";
    public static final String ACCESS_TOKEN = "access_token";
    public static final String EXPIRES_IN = "expires_in";
    public static final String DATE_CREATION = "date_creation";
    public static final String REFRESH_TOKEN = "refresh_token";
    public static final String TAG = "MainActivity";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Log.d(TAG, "onCreate: setContentView");
    }

    @Override
    protected void onResume() {
        super.onResume();
        assert getSupportActionBar() != null;   //null check
        this.getSupportActionBar().hide();

        Log.d(TAG, "onCreate: actionbar");
        ProgressBar progressBar = findViewById(R.id.progressBar);
        TextInputLayout usernameTextInput = findViewById(R.id.username);
        TextInputLayout passwordTextInput = findViewById(R.id.password);
        Button logButton = findViewById(R.id.loginButton);
        ImageView imageView = findViewById(R.id.image);
        LinearLayout linearLayoutForm = findViewById(R.id.form);


        try {
            if (isLoggedIn()) {
                Log.d(TAG, "onCreate: connected");
            } else {
                imageView.setVisibility(View.VISIBLE);
                progressBar.setVisibility(View.GONE);
                linearLayoutForm.setVisibility(View.VISIBLE);
                Log.d(TAG, "onCreate: not connected");
            }
        } catch (ExecutionException | InterruptedException e) {
            e.printStackTrace();
        }


        logButton.setOnClickListener(v -> {
            imageView.setVisibility(View.GONE);
            progressBar.setVisibility(View.VISIBLE);
            progressBar.setProgress(0);
            UserLoginTask userLoginTask = new UserLoginTask(usernameTextInput.getEditText().getText().toString(), passwordTextInput.getEditText().getText().toString(), v.getContext());
            userLoginTask.execute();

        });
    }

    public String loadToken() {
        String token;
        SharedPreferences sharedPreferences = getSharedPreferences(SHARED_PREFS, MODE_PRIVATE);
        token = sharedPreferences.getString(ACCESS_TOKEN, null);
        return token;
    }

    public void logout() {
        SharedPreferences sharedPreferences = getSharedPreferences(SHARED_PREFS, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.remove(ACCESS_TOKEN);
        editor.remove(DATE_CREATION);
        editor.remove(REFRESH_TOKEN);
        editor.apply();
    }

    public void saveToken(Token token) {
        SharedPreferences sharedPreferences = getSharedPreferences(SHARED_PREFS, Context.MODE_PRIVATE);


        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(ACCESS_TOKEN, token.getAccess_token());
        editor.putLong(EXPIRES_IN, token.getExpires_in());
        editor.putString(REFRESH_TOKEN, token.getRefresh_token());
        editor.apply();
    }

    public Boolean isLoggedIn() throws ExecutionException, InterruptedException {
        SharedPreferences sharedPreferences = getSharedPreferences(SHARED_PREFS, MODE_PRIVATE);
        String refresh_token;
        refresh_token = sharedPreferences.getString(REFRESH_TOKEN, null);

        if (refresh_token != null) {

            UserLoginTask userLoginTask = new UserLoginTask(refresh_token, this);
            userLoginTask.execute();

            return true;
        } else {
            return false;
        }

    }
}


class UserLoginTask extends AsyncTask<Void, Void, Token> {

    private final String mUsername;
    private final String refresh_token;
    private final String mPassword;
    private final Context context;

    public UserLoginTask(String refresh_token, Context context) {
        this.context = context;
        this.mUsername = "";
        this.mPassword = "";
        this.refresh_token = refresh_token;
    }

    public UserLoginTask(String mUsername, String mPassword, Context context) {
        this.mUsername = mUsername;
        this.context = context;
        refresh_token = null;
        this.mPassword = mPassword;
    }

    @Override
    protected Token doInBackground(Void... voids) {

        Log.d("UserLoginTask", "I'm here top");

        OkHttpClient client = new OkHttpClient().newBuilder()
                .build();
        MediaType mediaType = MediaType.parse("application/json,text/plain");
        RequestBody body;
        if (refresh_token == null) {
            body = RequestBody.create(mediaType, "{\n\t\"grant_type\":\"password\",\n\t\"username\": \"" + mUsername + "\" ,\n\t\"password\": \"" + mPassword + "\", \n\t\"scope\": \"*\", \n\t\"client_id\": \"WMZNSSETCJDPTZSVETRNOPGYFKMAKHHQ\", \n\t\"client_secret\": \"5813427175e8e5d18452a90035077331\"\n}");
        } else {

            body = RequestBody.create(mediaType, "{\r\n    \"grant_type\":\"refresh_token\",\r\n    \"refresh_token\": \"" + refresh_token + "\",\r\n    \"scope\": \"*\",\r\n    \"client_id\": \"WMZNSSETCJDPTZSVETRNOPGYFKMAKHHQ\",\r\n    \"client_secret\": \"5813427175e8e5d18452a90035077331\"\r\n}");
        }

        Request request = new Request.Builder()
                .url("http://process.isiforge.tn/isi/oauth2/token")
                .method("POST", body)
                .addHeader("Content-Type", "application/json")
                .addHeader("charset", "UTF-8")
                .build();

        Log.d("UserLoginTask", "I'm here just before the response");
        try {
            Response response = client.newCall(request).execute();

            if (response.isSuccessful()) {

                String resp = response.body().string();
                Log.d("Response", "doInBackground: " + resp);
                JSONObject object = new JSONObject(resp);
                //String token = (String) object.get("access_token");

                Token token = new Token((String) object.getString("access_token"), object.getLong("expires_in"), object.getString("refresh_token"));

                return token;
            } else {
                Log.d("UserLoginTask", response.body().string());

                return null;
            }
        } catch (IOException e) {
            Log.d("UserLoginTask", "error execute");

            e.printStackTrace();
            return null;
        } catch (JSONException e) {
            Log.d("UserLoginTask", "error nextValue");
            e.printStackTrace();
            return null;
        }
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();


    }

    @Override
    protected void onPostExecute(Token token) {

        ProgressBar progressBar = ((MainActivity) context).findViewById(R.id.progressBar);
        TextInputLayout usernameTextInput = ((MainActivity) context).findViewById(R.id.username);
        TextInputLayout passwordTextInput = ((MainActivity) context).findViewById(R.id.password);
        Button logButton = ((MainActivity) context).findViewById(R.id.loginButton);
        ImageView imageView = ((MainActivity) context).findViewById(R.id.image);
        LinearLayout linearLayoutForm = ((MainActivity) context).findViewById(R.id.form);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            usernameTextInput.setAutofillHints(View.AUTOFILL_HINT_USERNAME);
            passwordTextInput.setAutofillHints(View.AUTOFILL_HINT_PASSWORD);
        }
        if (token != null) {
            super.onPostExecute(token);

            imageView.setVisibility(View.VISIBLE);
            progressBar.setVisibility(View.GONE);
            saveToken(token);


            Intent intent = new Intent(context, CasesListe.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK); // Adds the FLAG_ACTIVITY_NO_HISTORY flag
            context.startActivity(intent);
            ((MainActivity) context).finish();

        } else {
            if (refresh_token == null) {
                usernameTextInput.setError(" ");
                usernameTextInput.setErrorEnabled(true);
                passwordTextInput.setError("Nom d'utilisateur/mot de passe est incorrect");
                usernameTextInput.setErrorEnabled(true);
                linearLayoutForm.setVisibility(View.VISIBLE);
                imageView.setVisibility(View.VISIBLE);
                progressBar.setVisibility(View.GONE);
            } else {
                Toast.makeText(context, "Une erreur inattendue s’est produite!", Toast.LENGTH_LONG).show();
                linearLayoutForm.setVisibility(View.VISIBLE);
                imageView.setVisibility(View.VISIBLE);
                progressBar.setVisibility(View.GONE);

            }

        }
    }

    public void saveToken(Token token) {
        SharedPreferences sharedPreferences = context.getSharedPreferences(MainActivity.SHARED_PREFS, Context.MODE_PRIVATE);


        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(MainActivity.ACCESS_TOKEN, token.getAccess_token());
        editor.putLong(MainActivity.EXPIRES_IN, token.getExpires_in());
        editor.putString(MainActivity.REFRESH_TOKEN, token.getRefresh_token());
        editor.apply();
    }

}