package com.lachkham.processapp.model;

import android.content.Context;
import android.util.Log;
import android.widget.ArrayAdapter;
import android.widget.Filter;
import android.widget.Filterable;

import java.util.ArrayList;

public class ArrayAdapterAll extends ArrayAdapter<String> implements Filterable {

    ArrayList<String> _items = new ArrayList<String>();
    ArrayList<String> orig = new ArrayList<String>();

    public ArrayAdapterAll(Context context, int resource, ArrayList<String> items) {
        super(context, resource, items);

        for (int i = 0; i < items.size(); i++) {
            orig.add(items.get(i));
        }
    }

    @Override
    public int getCount() {
        if (_items != null)
            return _items.size();
        else
            return 0;
    }

    @Override
    public String getItem(int arg0) {
        return _items.get(arg0);
    }

    @Override
    public Filter getFilter() {
        Filter filter = new Filter() {

            @Override
            protected FilterResults performFiltering(CharSequence constraint) {

                Log.d("filter", "performFiltering: ");
                FilterResults oReturn = new FilterResults();
                _items.clear();
                _items = orig;
                oReturn.values = _items;
                oReturn.count = _items.size();

                return oReturn;
            }


            @Override
            protected void publishResults(CharSequence constraint, FilterResults results) {
                if (results != null && results.count > 0) {
                    notifyDataSetChanged();
                } else {
                    notifyDataSetInvalidated();
                }

            }

        };

        return filter;

    }


}
