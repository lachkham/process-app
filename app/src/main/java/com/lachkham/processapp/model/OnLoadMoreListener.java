package com.lachkham.processapp.model;


public interface OnLoadMoreListener {
    void onLoadMore();
}