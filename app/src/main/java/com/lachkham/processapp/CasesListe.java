package com.lachkham.processapp;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.material.navigation.NavigationView;
import com.lachkham.processapp.fragments.ParticipatedFragment;
import com.lachkham.processapp.fragments.WkfFragment;
import com.lachkham.processapp.model.Wkf;

import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONTokener;

import java.io.IOException;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.fragment.app.FragmentTransaction;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

import static com.lachkham.processapp.MainActivity.SHARED_PREFS;

public class CasesListe extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener, WkfFragment.OnListFragmentInteractionListener, ParticipatedFragment.OnListFragmentInteractionListener {
    private DrawerLayout drawerLayout;

    private String token;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cases_liste);
        SwipeRefreshLayout swipeContainer = findViewById(R.id.swipeContainer);
        swipeContainer.setEnabled(false);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        Intent intent = getIntent();
        token = intent.getStringExtra("token");

        drawerLayout = findViewById(R.id.drawer_layout);
        NavigationView navigationView = findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(this, drawerLayout, toolbar, R.string.nav_app_bar_open_drawer_description, R.string.nav_app_bar_open_drawer_description);
        drawerLayout.addDrawerListener(toggle);
        toggle.syncState();

        if (savedInstanceState == null) {
            getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container, new WkfFragment()).commit();
            navigationView.setCheckedItem(R.id.nav_home);
        }
        LoggedInUserInformation loggedInUserInformation = new LoggedInUserInformation(loadToken(), this);
        loggedInUserInformation.execute();
    }


    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()) {
            case R.id.nav_home:
                FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
                ft.replace(R.id.fragment_container, new WkfFragment()).commit();
                ft.addToBackStack(null);
                getSupportActionBar().setTitle(R.string.demandes);
                break;

            case R.id.nav_suivi:
                FragmentTransaction ft1 = getSupportFragmentManager().beginTransaction();
                ft1.replace(R.id.fragment_container, new ParticipatedFragment()).commit();
                ft1.addToBackStack(null);
                getSupportActionBar().setTitle(R.string.suivi);

                break;
            case R.id.nav_logout:
                logout();
                Toast.makeText(this, "Déconnexion", Toast.LENGTH_LONG).show();
                break;

        }
        drawerLayout.closeDrawer(GravityCompat.START);
        return true;
    }

    public String loadToken() {
        String token;

        SharedPreferences sharedPreferences = getSharedPreferences(SHARED_PREFS, MainActivity.MODE_PRIVATE);
        token = sharedPreferences.getString(MainActivity.ACCESS_TOKEN, null);
        return token;
    }

    @Override
    public void onBackPressed() {
        if (drawerLayout.isDrawerOpen(GravityCompat.START)) {
            drawerLayout.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public void onListFragmentInteraction(Wkf item) {
    }


    public void logout() {
        SharedPreferences sharedPreferences = getSharedPreferences(MainActivity.SHARED_PREFS, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.remove(MainActivity.ACCESS_TOKEN);
        editor.remove(MainActivity.DATE_CREATION);
        editor.remove(MainActivity.REFRESH_TOKEN);
        editor.commit();
        Intent intent = new Intent(this, MainActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK); // Adds the FLAG_ACTIVITY_NO_HISTORY flag
        startActivity(intent);
        CasesListe.this.finish();
    }


    @Override
    public void onListFragmentInteraction(JSONObject item) {

    }
}

class LoggedInUserInformation extends AsyncTask<Void, Void, JSONObject> {

    private static final String TAG = "LoggedInUserInformation";
    private String token;
    private Context context;

    LoggedInUserInformation(String token, Context context) {
        this.token = token;
        this.context = context;
    }


    @Override
    protected JSONObject doInBackground(Void... voids) {
        OkHttpClient client = new OkHttpClient().newBuilder()
                .build();
        Request request = new Request.Builder()
                .url("http://process.isiforge.tn/api/1.0/isi/extrarest/login-user")
                .method("GET", null)
                .addHeader("Authorization", "Bearer " + token)
                .build();
        try {
            Response response = client.newCall(request).execute();
            if (response.isSuccessful()) {
                String resp = response.body().string();
                JSONObject object = (JSONObject) new JSONTokener(resp).nextValue();
                Log.d(TAG, "doInBackground: " + object.toString());
                return object;
            }
        } catch (IOException | JSONException e) {
            Log.d(TAG, "doInBackground: " + e);
            e.printStackTrace();
        }
        Log.d(TAG, "doInBackground: Error");
        return null;
    }


    @Override
    protected void onPostExecute(JSONObject jsonObject) {
        super.onPostExecute(jsonObject);
        if (jsonObject != null) {
            NavigationView navigationView = (NavigationView) ((CasesListe) context).findViewById(R.id.nav_view);
            View headerView = navigationView.getHeaderView(0);
            TextView firtlastname = headerView.findViewById(R.id.firtlastname);
            TextView useremail = headerView.findViewById(R.id.useremail);
            try {
                firtlastname.setText(String.format("%s %s ", jsonObject.getString("firstname"), jsonObject.getString("lastname")));
                useremail.setText(jsonObject.getString("mail"));
            } catch (JSONException e) {
                e.printStackTrace();
            }
        } else {
            Toast.makeText(context, "Une erreur inattendue s’est produite!", Toast.LENGTH_LONG).show();
            Intent intent = new Intent(((CasesListe) context), MainActivity.class);
            ((CasesListe) context).startActivity(intent);
        }
    }
}



