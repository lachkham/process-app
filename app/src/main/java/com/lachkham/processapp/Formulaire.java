package com.lachkham.processapp;

import android.annotation.SuppressLint;
import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.ColorDrawable;
import android.icu.util.Calendar;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.google.android.material.dialog.MaterialAlertDialogBuilder;
import com.google.android.material.textfield.TextInputLayout;
import com.google.gson.JsonArray;
import com.lachkham.processapp.model.ArrayAdapterAll;
import com.lachkham.processapp.model.Wkf;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Objects;
import java.util.concurrent.ExecutionException;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

import static com.lachkham.processapp.MainActivity.SHARED_PREFS;

public class Formulaire extends AppCompatActivity {

    public static String TAG = "Formulaire";

    public Wkf task;
    private JSONArray steps;
    public static AlertDialog alertDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_formulaire);
        assert getSupportActionBar() != null;   //null check
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("Formulaire");

        Intent intent = getIntent();
        String app_uid = intent.getStringExtra("app_uid");
        ColorDrawable cd = new ColorDrawable(0x467fe7);

        alertDialog = new MaterialAlertDialogBuilder(this)
                .setBackground(cd)
                .setView(R.layout.loading_view)
                .setCancelable(false)
                .show();


        task = new Wkf(intent.getStringExtra("tas_uid"), intent.getStringExtra("Pro_title"), intent.getStringExtra("Pro_uid"));

        StepsTask dynaformTask = new StepsTask(loadToken(), task, app_uid, this);
        dynaformTask.execute();


    }

    @Override
    public boolean onSupportNavigateUp() {
        finish();
        return true;
    }

    public String loadToken() {
        String token;

        SharedPreferences sharedPreferences = getSharedPreferences(SHARED_PREFS, MainActivity.MODE_PRIVATE);
        token = sharedPreferences.getString(MainActivity.ACCESS_TOKEN, null);
        return token;
    }
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////
class DynaFormTask extends AsyncTask<Void, Void, JSONArray> {


    private final Wkf task;
    private final String app_uid;
    private final JSONArray jsonArray;
    private final int step;
    ArrayList<String> var = new ArrayList<String>();
    private String token;
    @SuppressLint("StaticFieldLeak")
    private Context context;
    private DatePickerDialog datePicker;
    private TimePickerDialog timePicker;


    public DynaFormTask(String token, Wkf task, Context context, String app_uid, JSONArray jsonArray, int step) {
        this.token = token;
        this.task = task;
        this.context = context;
        this.app_uid = app_uid;
        this.jsonArray = jsonArray;
        this.step = step;
    }

    @Override
    protected JSONArray doInBackground(Void... voids) {
        JSONObject jsonObject = new JSONObject();
        JSONArray jsonArray = new JSONArray();

        OkHttpClient client = new OkHttpClient().newBuilder()
                .build();
        Request request = null;
        try {
            request = new Request.Builder()
                    .url("http://process.isiforge.tn/api/1.0/isi/extrarest/dynaform/" + this.jsonArray.getJSONObject(step).getString("step_uid_obj"))
                    .method("GET", null)
                    .addHeader("Authorization", "Bearer " + token)
                    .build();
        } catch (JSONException e) {
            e.printStackTrace();
        }

        try {
            Response response = client.newCall(request).execute();
            if (response.isSuccessful()) {
                String resp = response.body().string();
                JSONArray jObj = new JSONArray(resp);
                jsonArray.put(jObj);
                Log.d("Formulaire", "doInBackground: " + jsonObject.toString());
                if (app_uid != null) {
                    OkHttpClient client1 = new OkHttpClient().newBuilder()
                            .build();
                    Request request1 = new Request.Builder()
                            .url("http://process.isiforge.tn/api/1.0/isi/cases/" + app_uid + "/variables")
                            .method("GET", null)
                            .addHeader("Authorization", "Bearer " + token)
                            .build();
                    Response response1 = client1.newCall(request1).execute();
                    String resp1 = response1.body().string();
                    jsonObject = new JSONObject(resp1);
                    jsonArray.put(jsonObject);
                }
                return jsonArray;

            } else {
                return null;
            }


        } catch (IOException | JSONException e) {
            e.printStackTrace();
            return null;

        }


    }


    @Override
    protected void onPostExecute(JSONArray jsonArray) {
        super.onPostExecute(jsonArray);

        JSONObject app_uid_Object = null;
        if (jsonArray != null) {

            try {
                if (app_uid != null) {
                    app_uid_Object = jsonArray.getJSONObject(1);
                    Log.d("app_uid_Object", "onPostExecute: "+app_uid_Object);
                }


                jsonArray = jsonArray.getJSONArray(0);

                Log.d("Response", jsonArray.toString());
                LinearLayout linearLayout;
                linearLayout = ((Formulaire) context).findViewById(R.id.form);
                linearLayout.setPadding(16, 16, 16, 16);
                for (int i = 0; i < jsonArray.length(); i++) {
                    try {
                        addToForm(jsonArray.getJSONObject(i), linearLayout, app_uid_Object);
                    } catch (JSONException e) {
                        Log.d("Response", "onPostExecute: not a form field : " + i);
                    } catch (InterruptedException | ExecutionException | IOException e) {
                        e.printStackTrace();
                    }
                }

                Formulaire.alertDialog.dismiss();
                linearLayout.setVisibility(View.VISIBLE);

            } catch (JSONException e) {
                e.printStackTrace();
            }
        } else {
            ((Formulaire) context).finish();
        }
    }

    @SuppressLint({"DefaultLocale", "ResourceType"})
    private void addToForm(JSONObject jsonObject, LinearLayout linearLayout, JSONObject app_uid_Object) throws JSONException, ExecutionException, InterruptedException, IOException {
        //Log.d("Form field", "addToForm: "+jsonObject.getString("type"));
        switch (jsonObject.getString("type")) {
            case "image":
                ImageView imageView = new ImageView((Formulaire)context);
                URL url = new URL(jsonObject.getString("src"));
                Bitmap bmp = (new ImageTask(url).execute().get());
                LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams((int)  context.getResources().getDimension(R.dimen.imageview_width),(int)  context.getResources().getDimension(R.dimen.imageview_height));
                imageView.setLayoutParams(layoutParams);
                imageView.setImageBitmap(bmp);
                linearLayout.addView(imageView);


                break;
            case "text":
                LayoutInflater textInflater = LayoutInflater.from(((Formulaire) context));
                TextInputLayout textInputLayout = (TextInputLayout) textInflater.inflate(R.layout.material_edit_text, null);


                if (jsonObject.getBoolean("required")) {
                    textInputLayout.setHint(jsonObject.getString("label") + " (*)");
                    textInputLayout.setContentDescription(jsonObject.getString("label") + " (*): est obligatoire");

                } else {
                    textInputLayout.setHint(jsonObject.getString("label"));
                }
                linearLayout.addView(textInputLayout);
                if (app_uid != null && app_uid_Object!=null) {
                    textInputLayout.getEditText().setText(app_uid_Object.getString(jsonObject.getString("variable")));
                }
                if(!jsonObject.getString("variable").isEmpty()){
                    textInputLayout.setTag(jsonObject.getString("variable"));
                }else{
                    textInputLayout.setTag(jsonObject.getString("id"));
                }
                break;
            case "textarea":
                LayoutInflater textareaInflater = LayoutInflater.from(((Formulaire) context));
                TextInputLayout textareaInputLayout = (TextInputLayout) textareaInflater.inflate(R.layout.material_textarea, null);
                if(!jsonObject.getString("variable").isEmpty()){
                    textareaInputLayout.setTag(jsonObject.getString("variable"));
                }else{
                    textareaInputLayout.setTag(jsonObject.getString("id"));
                }
                if (jsonObject.getBoolean("required")) {
                    textareaInputLayout.setHint(jsonObject.getString("label") + " (*)");
                    textareaInputLayout.setContentDescription(jsonObject.getString("label") + " (*): est obligatoire");

                } else {
                    textareaInputLayout.setHint(jsonObject.getString("label"));
                }
                linearLayout.addView(textareaInputLayout);

                if (app_uid != null && app_uid_Object!=null) {
                    textareaInputLayout.getEditText().setText(app_uid_Object.getString(jsonObject.getString("variable")));
                }
                break;
            case "dropdown":
                LayoutInflater dropdowInflater = LayoutInflater.from(((Formulaire) context));
                TextInputLayout dropdown = (TextInputLayout) dropdowInflater.inflate(R.layout.material_dropdown, null);
                dropdown.setTag(jsonObject.getString("variable"));
                if(!jsonObject.getString("variable").isEmpty()){
                    dropdown.setTag(jsonObject.getString("variable"));
                }else{
                    dropdown.setTag(jsonObject.getString("id"));
                }

                linearLayout.addView(dropdown);

                JSONArray options = jsonObject.getJSONArray("options");
                if(options.length()==0) {
                    QueryTask queryTask = new QueryTask(token,task,this.jsonArray.getJSONObject(step).getString("step_uid_obj"),jsonObject.getString("variable"));
                     options= queryTask.execute().get();
                }
                    ArrayList<String> items = new ArrayList<>();
                    ArrayList<String> tag = new ArrayList<>();
                    for (int i = 0; i < options.length(); i++) {
                            try{
                                items.add( options.getJSONObject(i).getString("value")+"   |   "+options.getJSONObject(i).getString("text"));
                            }catch (JSONException j) {
                                items.add(options.getJSONObject(i).getString("value"));
                            }
                        tag.add(options.getJSONObject(i).getString("value"));


                    }
                    //Log.d("dropdown", Arrays.toString(items));

                    ArrayAdapter<String> adapter = new ArrayAdapterAll(((Formulaire) context), R.layout.list_item, items);
                    AutoCompleteTextView editTextFilledExposedDropdown = (AutoCompleteTextView) dropdown.getEditText();
                    editTextFilledExposedDropdown.setAdapter(adapter);


                if (jsonObject.getBoolean("required")) {
                    dropdown.setHint(jsonObject.getString("label") + " (*)");
                    dropdown.setContentDescription(jsonObject.getString("label") + " (*): est obligatoire");

                } else {
                    dropdown.setHint(jsonObject.getString("label"));
                }




                if (app_uid != null && app_uid_Object!=null) {
                    try {
                        Log.d("Formulaire", "addToForm: "+ tag.indexOf(app_uid_Object.getString(jsonObject.getString("variable"))));
                        Log.d("Formulaire", "addToForm: "+ app_uid_Object);
                        try{
                            dropdown.getEditText().setText(items.get(tag.indexOf(app_uid_Object.getString(jsonObject.getString("variable")))));

                        }catch (Exception e){
                            e.printStackTrace();
                        }

                    } catch (JSONException e) {
                        dropdown.getEditText().setText("");
                    }
                } else {
                    dropdown.getEditText().setText("");
                }

                break;
            case "title":
            case "subtitle":
                LayoutInflater titleInflater = LayoutInflater.from(((Formulaire) context));
                TextView titleLayout = (TextView) titleInflater.inflate(R.layout.material_title, null);
                titleLayout.setText(jsonObject.getString("label"));
                linearLayout.addView(titleLayout);
                break;


            case "datetime":
                //TODO:  date format -> YYYY-MM-DD ->date Picker
                //TODO:              -> hh:mm      ->time Picker
                LayoutInflater dateInflater = LayoutInflater.from(((Formulaire) context));
                TextInputLayout dateInputLayout = (TextInputLayout) dateInflater.inflate(R.layout.material_datepicker, null);
                if(!jsonObject.getString("variable").isEmpty()){
                    dateInputLayout.setTag(jsonObject.getString("variable"));
                }else{
                    dateInputLayout.setTag(jsonObject.getString("id"));
                }
                if (jsonObject.getBoolean("required")) {
                    dateInputLayout.setHint(jsonObject.getString("label") + " (*)");
                    dateInputLayout.setContentDescription(jsonObject.getString("label") + " (*): est obligatoire");
                } else {
                    dateInputLayout.setHint(jsonObject.getString("label"));
                }

                linearLayout.addView(dateInputLayout);

                if (jsonObject.getString("format").equals("YYYY-MM-DD")) {
                    Log.d("datetime", "YYYY-MM-DD: ");
                    if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {

                        dateInputLayout.setEndIconOnClickListener(v -> {
                        final Calendar cldr = Calendar.getInstance();
                        int day = cldr.get(Calendar.DAY_OF_MONTH);
                        int month = cldr.get(Calendar.MONTH);
                        int year = cldr.get(Calendar.YEAR);
                        Log.d("datetime", "onCLick ");
                        datePicker = new DatePickerDialog(context, (view, year1, month1, dayOfMonth) -> {
                            Objects.requireNonNull(dateInputLayout.getEditText()).setText(String.format("%d/%d/%d", dayOfMonth, month1 + 1, year1));
                        }, year, month, day);
                        datePicker.show();
                    });
                    }
                } else {

                    Log.d("datetime", "else: ");
                    if (jsonObject.getString("format").equals("hh:mm")) {
                        dateInputLayout.setEndIconDrawable(R.drawable.ic_access_time_black_24dp);
                        Log.d("datetime", "addToForm: hh:mm ");
                        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {
                            dateInputLayout.setEndIconOnClickListener(v -> {

                            final Calendar c = Calendar.getInstance();
                            int hour = c.get(Calendar.HOUR_OF_DAY);
                            int minute = c.get(Calendar.MINUTE);
                            Log.d("datetime", "onCLick ");
                            timePicker = new TimePickerDialog(context, new TimePickerDialog.OnTimeSetListener() {
                                @Override
                                public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
                                    Objects.requireNonNull(dateInputLayout.getEditText()).setText(String.format("%d:%d", hourOfDay, minute));
                                }
                            }, hour, minute, true);
                            timePicker.show();
                        });}
                    }

                }
                if (app_uid != null && app_uid_Object!=null) {
                    dateInputLayout.getEditText().setText(app_uid_Object.getString(jsonObject.getString("variable")));
                }

                break;

            case "radio":
                LayoutInflater radioInflater = LayoutInflater.from(((Formulaire) context));
                RadioGroup radioGroup = (RadioGroup) radioInflater.inflate(R.layout.material_radio_group, null);
                LayoutInflater titleInflaterradio = LayoutInflater.from(((Formulaire) context));
                if(!jsonObject.getString("variable").isEmpty()){
                    radioGroup.setTag(jsonObject.getString("variable"));
                }else{
                    radioGroup.setTag(jsonObject.getString("id"));
                }
                TextView titleInputLayoutradio = (TextView) titleInflaterradio.inflate(R.layout.material_title, null);

                titleInputLayoutradio.setText(String.format("%s (*) ", jsonObject.getString("label")));

                linearLayout.addView(titleInputLayoutradio);

                linearLayout.addView(radioGroup);
                int pos = 1;
                JSONArray radio_options = jsonObject.getJSONArray("options");
                RadioGroup.LayoutParams params = new RadioGroup.LayoutParams(RadioGroup.LayoutParams.WRAP_CONTENT, RadioGroup.LayoutParams.WRAP_CONTENT);
                for (int i = 0; i < radio_options.length(); i++) {
                    RadioButton radioButton = new RadioButton(((Formulaire) context));
                    String opt = radio_options.getJSONObject(i).getString("value");
                    radioButton.setText(opt);
                    radioGroup.addView(radioButton, params);
                    if (i == 0) {
                        radioButton.setChecked(true);
                    }
                    try {
                        if (app_uid != null && app_uid_Object!=null && opt.equals(app_uid_Object.getString(jsonObject.getString("variable")))) {
                            radioButton.setChecked(true);
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                }


                break;

            case "submit":
                LayoutInflater buttonInflater = LayoutInflater.from(((Formulaire) context));
                Button button = (Button) buttonInflater.inflate(R.layout.material_button, null);
                button.setText(jsonObject.getString("label"));
                LinearLayout.LayoutParams button_params = new LinearLayout.LayoutParams(RadioGroup.LayoutParams.MATCH_PARENT, RadioGroup.LayoutParams.WRAP_CONTENT);
                linearLayout.addView(button, button_params);
                button.setOnClickListener(v -> {
                    try {
                        if(!jsonObject.getString("id").equals("cancelcase"))
                        submit(linearLayout);
                        else{
                            ((Formulaire)context).finish();
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                });

                break;
            case "button":
                LayoutInflater buttonInflater1 = LayoutInflater.from(((Formulaire) context));
                Button button1 = (Button) buttonInflater1.inflate(R.layout.material_button, null);
                button1.setText(jsonObject.getString("label"));
                LinearLayout.LayoutParams button_params1 = new LinearLayout.LayoutParams(RadioGroup.LayoutParams.MATCH_PARENT, RadioGroup.LayoutParams.WRAP_CONTENT);
                linearLayout.addView(button1, button_params1);
                button1.setOnClickListener(v -> {
                    try {
                        if(jsonObject.getString("id").equals("consulter"))
                            submit(linearLayout);
                        else{
                            ((Formulaire)context).finish();
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                });

                break;
            case "grid":
                LinearLayout subLinearlayout = new LinearLayout((Formulaire)context);
                subLinearlayout.setOrientation(LinearLayout.VERTICAL);
                linearLayout.addView(subLinearlayout);
                Log.d("grid", "addToForm: ///////////////////////////////////////////////////////////////////");
                subLinearlayout.setTag(jsonObject.getString("variable"));
                Log.d("grid", "addToForm:"+app_uid_Object);

                JSONArray jsonArray = jsonObject.getJSONArray("columns");

                for(int i = 0 ; i< jsonArray.length();i++){
                    Log.d("grid", "addToForm:"+ jsonArray.getJSONObject(i));
                    addToForm(jsonArray.getJSONObject(i) , subLinearlayout,  null);
                    try {
                        if(app_uid!=null&& app_uid_Object!=null){
                            JSONObject jsonObjectGrid=  app_uid_Object.getJSONObject((String) subLinearlayout.getTag());
                            TextInputLayout textInputLayoutGrid = (TextInputLayout) subLinearlayout.getChildAt(i);
                            Log.d("grid", "addToForm:"+ jsonObjectGrid.getJSONObject("1"));

                            Objects.requireNonNull(textInputLayoutGrid.getEditText()).setText(jsonObjectGrid.getJSONObject("1").getString(jsonArray.getJSONObject(i).getString("id")));

                        }
                    }catch (JSONException e){
                        e.printStackTrace();
                    }


                }
                break;


        }
        Log.d("default", jsonObject.getString("variable"));
        if(!jsonObject.getString("variable").isEmpty()){
            var.add(jsonObject.getString("variable"));
        }
        //  Log.d("default", id.toString());

    }

    private void submit(LinearLayout linearLayout) throws JSONException {
        final int childCount = linearLayout.getChildCount();
        Log.d("Submit", "submit: " + childCount);
        Log.d("Submit", "submit: " + task);
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("pro_uid", task.getPro_uid());
        jsonObject.put("tas_uid", task.getTas_uid());
        JSONObject variables = new JSONObject();
        int j = 0;
        Boolean required = true;
        for (int i = 0; i < childCount; i++) {
            View view = linearLayout.getChildAt(i);
            switch (view.getClass().getSimpleName()) {
                case "TextInputLayout":
                    if (((TextInputLayout) view).getContentDescription() != null && ((TextInputLayout) view).getEditText().getText().toString().equals("")) {
                        Log.d("submit", "submit: " + ((TextInputLayout) view).getError());
                        ((TextInputLayout) view).setError(((TextInputLayout) view).getContentDescription());
                        Objects.requireNonNull(((TextInputLayout) view).getEditText()).setOnFocusChangeListener((v, hasFocus) -> ((TextInputLayout) view).setErrorEnabled(false));
                        required = false;
                    } else {
                        ((TextInputLayout) view).setErrorEnabled(false);
                        String value = Objects.requireNonNull(((TextInputLayout) view).getEditText()).getText().toString();
                        if(value.contains("   |   ")){
                                value=value.substring(0,value.indexOf("   |   "));
                        }
                        variables.put((String) view.getTag(), value );
                    }
                    j++;
                    break;
                case "RadioGroup":

                    RadioButton radioButton = (RadioButton) ((Formulaire) context).findViewById(((RadioGroup) view).getCheckedRadioButtonId());
                    variables.put((String) view.getTag(), radioButton.getText());
                    j++;
                    break;
                case "LinearLayout":
                    LinearLayout linearLayoutGrid = (LinearLayout)view;
                    JSONObject jsonObjectGrid = new JSONObject();
                    JSONObject jsonObjectGridChild = new JSONObject();
                    for(int k=0; k< linearLayoutGrid.getChildCount();k++){
                        View view1 = linearLayoutGrid.getChildAt(k);
                        Log.d("submit", "view: " +view1.getClass().getSimpleName());
                        switch (view1.getClass().getSimpleName()) {
                            case "TextInputLayout":
                                if (((TextInputLayout) view1).getContentDescription() != null && ((TextInputLayout) view1).getEditText().getText().toString().equals("")) {
                                    Log.d("submit", "submit: " + ((TextInputLayout) view1).getError());
                                    ((TextInputLayout) view1).setError(((TextInputLayout) view1).getContentDescription());
                                    Objects.requireNonNull(((TextInputLayout) view1).getEditText()).setOnFocusChangeListener((v, hasFocus) -> ((TextInputLayout) view1).setErrorEnabled(false));
                                    required = false;
                                } else {
                                    ((TextInputLayout) view1).setErrorEnabled(false);
                                    String value = Objects.requireNonNull(((TextInputLayout) view1).getEditText()).getText().toString();
                                    if (value.contains("   |   ")) {
                                        value = value.substring(0, value.indexOf("   |   "));
                                    }
                                    Log.d("submit", "submit: TAG "+(String) view1.getTag()+" Value: "+value);
                                    jsonObjectGridChild.put((String) view1.getTag(), value);
                                }
                                j++;
                                break;
                            case "RadioGroup":

                                RadioButton radioButton1 = (RadioButton) ((Formulaire) context).findViewById(((RadioGroup) view1).getCheckedRadioButtonId());
                                jsonObjectGridChild.put((String) view1.getTag(), radioButton1.getText());
                                j++;
                                break;

                            default:
                        }
                    }
                    jsonObjectGrid.put("1",jsonObjectGridChild);
                    variables.put((String) view.getTag(),jsonObjectGrid);
                    Log.d("grid", "submit: "+variables);

                    break;
                default:
            }
        }

        jsonObject.put("variables", new JSONArray().put(variables));
        Log.d("submit", "submit: " + jsonObject.toString());


        if (required) {
            String message;
            if (app_uid == null) {
                message = "Envoyer cette demande?";
            } else {
                message = "Modifier cette demande?";
            }

            new MaterialAlertDialogBuilder(context)
                    .setTitle(message)

                    .setPositiveButton("Oui", (dialog, which) -> {
                        SubmitTask submitTask = new SubmitTask(token, jsonObject, app_uid, context, linearLayout);
                        submitTask.execute();
                    })
                    .setNegativeButton("Non", null)
                    .show();


        }
    }
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////////
class SubmitTask extends AsyncTask<Void, Void, JSONObject> {
    private final String token;
    private final JSONObject requestBody;
    private final String app_uid;
    private final Context context;
    private final LinearLayout linearLayout;

    SubmitTask(String token, JSONObject requestBody, String app_uid, Context context, LinearLayout linearLayout) {
        this.token = token;
        this.requestBody = requestBody;
        this.app_uid = app_uid;
        this.context = context;
        this.linearLayout = linearLayout;
    }

    @Override
    protected JSONObject doInBackground(Void... voids) {
        if (app_uid == null) {
            OkHttpClient client = new OkHttpClient().newBuilder()
                    .build();
            MediaType mediaType = MediaType.parse("application/json,text/plain");
            RequestBody body;
            body = RequestBody.create(mediaType, requestBody.toString());
            Request request = new Request.Builder()
                    .url("http://process.isiforge.tn/api/1.0/isi/cases")
                    .method("POST", body)
                    .addHeader("Authorization", "Bearer " + token)
                    .addHeader("Content-Type", "application/json")
                    .addHeader("charset", "UTF-8")
                    .build();
            try {
                Response response = client.newCall(request).execute();
                if (response.isSuccessful()) {
                    String resp = response.body().string();
                    JSONObject jObj = new JSONObject(resp);
                    return jObj;
                }
            } catch (IOException | JSONException e) {
                e.printStackTrace();
                return null;

            }

            return null;
        } else {
            try {
                OkHttpClient client = new OkHttpClient().newBuilder()
                        .build();
                MediaType mediaType = MediaType.parse("application/json");
                RequestBody body = RequestBody.create(mediaType, requestBody.getJSONArray("variables").getJSONObject(0).toString());
                Request request = new Request.Builder()
                        .url("http://process.isiforge.tn/api/1.0/isi/cases/" + app_uid + "/variable")
                        .method("PUT", body)
                        .addHeader("Authorization", "Bearer " + token)
                        .addHeader("Content-Type", "application/json")
                        .addHeader("charset", "UTF-8")
                        .build();
                try {
                    Response response = client.newCall(request).execute();
                    Log.d("Formulaire", "doInBackground: " + response.body().string());
                    if (response.isSuccessful()) {
                        Log.d("Formulaire", "doInBackground: Successfull");
                        JSONObject jObj = new JSONObject("{\"sucess\": true}");
                        return jObj;
                    }
                    Log.d("Formulaire", "doInBackground: notSuccessfull");

                } catch (IOException | JSONException e) {
                    e.printStackTrace();
                    return null;

                }

            } catch (JSONException e) {
                e.printStackTrace();
            }
            return null;
        }
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        Formulaire.alertDialog.show();
    }

    @Override
    protected void onPostExecute(JSONObject jsonObject) {
        super.onPostExecute(jsonObject);
        Formulaire.alertDialog.dismiss();

        if (jsonObject != null) {
            Log.d("submit", "onPostExecute: " + jsonObject.toString());
            if (app_uid == null) {
                try {
                    Toast.makeText(context, "votre demande N°" + jsonObject.getString("app_number") + "  a été enregistrée avec succès", Toast.LENGTH_LONG).show();
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                Intent intent = new Intent(context, CasesListe.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                context.startActivity(intent);
                ((Formulaire) context).finish();
            } else {
                ((Formulaire) context).finish();
                Toast.makeText(context, "La modification a été effectuée avec succès!", Toast.LENGTH_LONG).show();

            }
        } else {
            Toast.makeText(context, "Une erreur inattendue s’est produite!", Toast.LENGTH_LONG).show();
            Formulaire.alertDialog.dismiss();
        }

    }
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////////
class StepsTask extends AsyncTask<Void, Void, JSONArray> {


    private final String token;
    private final Wkf task;
    private final String app_uid;
    @SuppressLint("StaticFieldLeak")
    private Context context;


    public StepsTask(String token, Wkf task, String app_uid, Context context) {
        this.token = token;
        this.task = task;
        this.app_uid = app_uid;
        this.context = context;
    }

    @Override
    protected JSONArray doInBackground(Void... voids) {
        OkHttpClient client = new OkHttpClient().newBuilder()
                .build();
        Request request = new Request.Builder()
                .url("http://process.isiforge.tn/api/1.0/isi/project/" + task.getPro_uid() + "/activity/" + task.getTas_uid() + "/steps")
                .method("GET", null)
                .addHeader("Authorization", "Bearer " + token)
                .build();

        try {
            Response response = client.newCall(request).execute();
            if (response.isSuccessful()) {
                String resp = response.body().string();
                JSONArray jObj = new JSONArray(resp);
                return jObj;
            } else {
                return null;
            }


        } catch (IOException | JSONException e) {
            e.printStackTrace();
            return null;

        }


    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
    }

    @SuppressLint("SetTextI18n")
    @Override
    protected void onPostExecute(JSONArray jsonArray) {

        super.onPostExecute(jsonArray);
        if (jsonArray != null) {
            DynaFormTask dynaFormTask;

            try {
                Log.d("Formulaire", "doInBackground: task "+jsonArray);

                ((TextView) ((Formulaire) context).findViewById(R.id.formTitle)).setText(jsonArray.getJSONObject(0).getString("obj_title") + ": ");
                dynaFormTask = new DynaFormTask(token, task, context, app_uid, jsonArray, 0);

                dynaFormTask.execute();
            } catch (JSONException e) {
                e.printStackTrace();
            }


            Log.d("onPostExecute", jsonArray.toString());
        } else {
            ((Formulaire) context).finish();
        }
    }

}

//////////////////////////////////////////////////////////////////////////////////////////////////////////////
class QueryTask extends AsyncTask<Void, Void, JSONArray> {
    private final String token;
    private final Wkf task;
    private final String step;


    private final String variable;
    QueryTask(String token, Wkf task, String step,  String variable) {
        this.token = token;
        this.task = task;
        this.step = step;

        this.variable = variable;
    }

    @Override
    protected JSONArray doInBackground(Void... voids) {

        OkHttpClient client = new OkHttpClient().newBuilder()
                .build();
        MediaType mediaType = MediaType.parse("application/json");
        RequestBody body = RequestBody.create(mediaType, "{\n\t\"dyn_uid\":\""+step+"\",\n\t\"field_id\":\""+variable+"\"\n}");
        Request request = new Request.Builder()
                .url("http://process.isiforge.tn/api/1.0/isi/project/"+task.getPro_uid()+"/process-variable/"+variable+"/execute-query")
                .method("POST", body)
                .addHeader("Authorization", "Bearer "+token)
                .addHeader("Content-Type", "application/json")
                .build();
        try {
            Response response = client.newCall(request).execute();
            if (response.isSuccessful()) {
                assert response.body() != null;
                String resp = response.body().string();
                return new JSONArray(resp);
            } else {
                return null;
            }


        } catch (IOException | JSONException e) {
            e.printStackTrace();
            return null;

        }
    }


}
/////////////////////////////////////////////////////////////////////////////////

class ImageTask extends AsyncTask<Void, Void, Bitmap> {
private final URL url;

    ImageTask(URL url) {
        this.url = url;
    }

    @Override
    protected Bitmap doInBackground(Void... voids) {
        Bitmap bmp = null;
        try {
            bmp = BitmapFactory.decodeStream(url.openConnection().getInputStream());
        } catch (IOException e) {
            e.printStackTrace();
        }

        return bmp;
    }
}
